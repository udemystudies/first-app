import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductsService } from './services/products.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {
  products = [];
  productsSubscription = new Subscription();

  constructor(private productsService: ProductsService) {
  }

  ngOnInit() {
    this.products = this.productsService.getProducts();
    this.productsSubscription = this.productsService.productsUpdated.subscribe(() => {
      this.products = this.productsService.getProducts();
    });
  }

  ngOnDestroy(): void {
    this.productsSubscription.unsubscribe();
  }

  onAddProduct(form) {
    // this.products.push(this.productName);
    if (form.valid) {
      this.productsService.addProduct(form.value.productName);
      // this.products.push(form.values.productName);
    }
  }

  onRemoveProduct(productName: string) {
    // this.products = this.products.filter(product => product != productName);
  }

}
